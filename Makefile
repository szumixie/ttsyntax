all: ttsyntax.pdf

ttsyntax.pdf : ttsyntax.tex refs.bib
	lualatex ttsyntax; bibtex ttsyntax; lualatex ttsyntax; lualatex ttsyntax

clean:
	rm -f *.aux *.bbl *.blg *.fdb_latexmk *.fls *.log *.out *.nav *.snm *.toc *.vtc *.ptb *.xdv *~ *.pdf
